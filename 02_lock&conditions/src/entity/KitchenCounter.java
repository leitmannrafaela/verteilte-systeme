import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

//passsive Klasse
public class KitchenCounter {
    private int counter ;
    private int belegteSemmeln ;
    private Lock monitor = new ReentrantLock();
    private Condition voll = monitor.newCondition(); //sonder Warteschlange
    private Condition leer = monitor.newCondition();

    public KitchenCounter(int counter) {
        this.counter = counter;
    }

    public void put(){
        monitor.lock(); // Beginn des geschützen Bereichs
        try{
            while(belegteSemmeln == counter){ // while true, wenn ich warten muss
                try{
                    System.out.println("   Warte einen Moment um neue Semmeln abzulegen!- Gerade gibt es "+this.belegteSemmeln);
                    voll.await();
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
            belegteSemmeln +=1;
            System.out.println("Semmel wurde produziert. Es sind jetzt "+ belegteSemmeln+"auf der Theke");
            leer.signal();
        }finally {
            monitor.unlock(); // Ende des geschützen Bereichs
        }
    }

    public void take(){
        monitor.lock();
        try{
            while(belegteSemmeln == 0){
                try{
                    System.out.println("  Warte einen Moment bis neue Semmeln belegt wurden! Gerade gibt es "+ this.belegteSemmeln);
                    leer.await();
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
            belegteSemmeln -=1;
            System.out.println("Semmel wurde gekauft. Es sind jetzt "+ belegteSemmeln+"auf der Theke");
            voll.signal();
        }finally {
            monitor.unlock();
        }
    }

}
