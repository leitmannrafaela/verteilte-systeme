import java.util.Random;

//aktive Klasse
public class Student implements Runnable{
    private KitchenCounter theke;
    private String studentid;

    public Student(KitchenCounter theke, String studentid) {
        this.theke = theke;
        this .studentid = studentid;
    }

    @Override
    public void run(){
        while(true){
            theke.take();
            try{
                long time = new Random().nextInt(4000)+500;
                Thread.sleep(time);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}
