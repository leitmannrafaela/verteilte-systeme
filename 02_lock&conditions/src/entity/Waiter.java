import java.util.Random;

// aktive Klasse
public class Waiter implements Runnable{
    private KitchenCounter theke;
    private String kellnerid;

    public Waiter(KitchenCounter theke, String kellnerid) {
        this.theke = theke;
        this.kellnerid = kellnerid;
    }

    @Override
    public void run(){
        while(true){
            try{
                long time = new Random().nextInt(1000)+250;
                Thread.sleep(time);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            theke.put();
        }
    }
}
