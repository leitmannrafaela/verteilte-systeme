package de.client;

import de.lmu.api.BerichtIF;
import de.lmu.api.FrueherkennungsIF;
import de.lmu.entity.Roentgenbild;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Client {
    public static void main(String [] args){
        Roentgenbild rb = new Roentgenbild();
        rb.setPatientenName("Ahmad");

        try {
            Registry registry = LocateRegistry.getRegistry("localhost",1099);
            FrueherkennungsIF stub = (FrueherkennungsIF) registry.lookup("Online");

            BerichtIF bericht = stub.analysieren(rb);

            System.out.println("Ausgabe:"+bericht);
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }
}
