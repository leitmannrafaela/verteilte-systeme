package de.lmu.impl;

import de.lmu.api.BerichtIF;
import de.lmu.api.FrueherkennungsIF;
import de.lmu.entity.Roentgenbild;
import de.lmu.entity.Bericht;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class FrueherkennungsService implements FrueherkennungsIF {
    @Override
    public BerichtIF analysieren(Roentgenbild rb) throws RemoteException{
        System.out.println("Bericht"+rb.toString());

        Bericht antwort = new Bericht();
        antwort.setDiagnose("Kopfschmerzen");
        antwort.setWeiteresVorgehen("Ausruhen");

        BerichtIF berichtStub = (BerichtIF) UnicastRemoteObject.exportObject(antwort,0);
        System.out.println("Sende "+ antwort);
        return berichtStub;
    }
}
