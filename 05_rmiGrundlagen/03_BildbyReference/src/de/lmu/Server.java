package de;

import de.lmu.api.FrueherkennungsIF;
import de.lmu.impl.FrueherkennungsService;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server {
    public static void main(String [] args){
        FrueherkennungsIF serverImpl = new FrueherkennungsService();
        try {
            FrueherkennungsIF stub = (FrueherkennungsIF) UnicastRemoteObject.exportObject(serverImpl, 0);
            Registry registry = LocateRegistry.createRegistry(1099);
            registry.bind("Online", stub);
            System.out.println("Server ist bereit fuer Verbindung...");
        } catch (RemoteException | AlreadyBoundException e) {
            e.printStackTrace();
        }
    }
}
