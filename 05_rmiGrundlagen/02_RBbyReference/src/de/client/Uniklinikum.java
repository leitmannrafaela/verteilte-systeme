package de.client;

import de.lmu.api.FrueherkennungsIF;
import de.lmu.api.RoentgenbildIF;
import de.lmu.entity.Roentgenbild;
import de.lmu.entity.Bericht;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;


public class Uniklinikum {
    public static void main(String [] args) {
        Roentgenbild rb = new Roentgenbild();
        rb.setPrivateName("Ahmad");
        try{
            Registry registry = LocateRegistry.getRegistry("localhost", 1099);
            FrueherkennungsIF stub = (FrueherkennungsIF) registry.lookup("Online");

            RoentgenbildIF rbStub = (RoentgenbildIF) UnicastRemoteObject.exportObject(rb, 0);
            Bericht bericht = stub.analysieren(rbStub);

            System.out.println("Bericht empfangen: "+bericht );
        }catch (RemoteException | NotBoundException e){
            e.printStackTrace();
        }
    }

}
