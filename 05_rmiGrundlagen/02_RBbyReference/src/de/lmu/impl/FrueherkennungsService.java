package de.lmu.impl;

import de.lmu.api.FrueherkennungsIF;
import de.lmu.api.RoentgenbildIF;
import de.lmu.entity.Bericht;

import java.rmi.RemoteException;

public class FrueherkennungsService implements FrueherkennungsIF {
    @Override
    public Bericht analysieren(RoentgenbildIF rb) throws RemoteException {
        System.out.println("Bericht: "+ rb.toString());

        Bericht antwort = new Bericht();
        antwort.setDiagnose("Kopfschmerzen");
        antwort.setWeiteresVorgehen("Ausruhen");

        System.out.println("Sende "+ antwort );
        return  antwort;
    }
}
