package de.lmu.api;

import de.lmu.entity.Bericht;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface FrueherkennungsIF extends Remote {
    Bericht analysieren(RoentgenbildIF rb) throws RemoteException;
}
