package de.lmu.api;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;

public interface RoentgenbildIF extends Remote {
    String getPrivateName()throws RemoteException;
    Date getAufnahmeVom() throws RemoteException;
    byte[] getRawData() throws RemoteException;
}
