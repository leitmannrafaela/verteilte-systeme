package de.lmu.entity;

import java.io.Serializable;
import java.util.Date;

public class Bericht implements Serializable {
    //Attribute
    private Date datum;
    private String diagnose;
    private String weiteresVorgehen;

    public Bericht() {
        this.datum = new Date();
    }

    public void setDiagnose(String diagnose) {
        this.diagnose = diagnose;
    }

    public void setWeiteresVorgehen(String weiteresVorgehen) {
        this.weiteresVorgehen = weiteresVorgehen;
    }

    @Override
    public String toString() {
        return "Bericht{" +
                "datum=" + datum +
                ", diagnose='" + diagnose + '\'' +
                ", weiteresVorgehen='" + weiteresVorgehen + '\'' +
                '}';
    }
}
