package de.lmu.entity;

import de.lmu.api.RoentgenbildIF;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Date;

public class Roentgenbild implements RoentgenbildIF {
    //Attribute
    private Date aufnahmeVom;
    private transient String privateName; //wird nicht serialisiert
    private byte[] rawData;

    public Roentgenbild() {
        this.aufnahmeVom = new Date();
        this.rawData = "Bild mit ".getBytes();
    }

    @Override
    public String getPrivateName() throws RemoteException {
        return this.privateName;
    }

    @Override
    public Date getAufnahmeVom()  {
        System.out.println("getAufnahmeDatum() aufgerufen!");
        return aufnahmeVom;
    }

    @Override
    public byte[] getRawData() {
        return rawData;
    }

    public void setAufnahmeVom(Date aufnahmeVom) {
        this.aufnahmeVom = aufnahmeVom;
    }

    public void setRawData(byte[] rawData) {
        this.rawData = rawData;
    }

    public void setPrivateName(String privateName) {
        this.privateName = privateName;
    }

    @Override
    public String toString() {
        return "Roentgenbild{" +
                "aufnahmeVom=" + aufnahmeVom +
                ", privateName='" + privateName + '\'' +
                ", rawData=" + Arrays.toString(rawData) +
                '}';
    }
}
