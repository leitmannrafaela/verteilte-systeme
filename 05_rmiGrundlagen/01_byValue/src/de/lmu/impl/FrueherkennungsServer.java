package de.lmu.impl;

import de.lmu.api.FrueherkennungsIF;
import de.lmu.entity.Bericht;
import de.lmu.entity.Roentgenbild;

import java.rmi.RemoteException;


public class FrueherkennungsServer implements FrueherkennungsIF {
    @Override
    public Bericht analysieren(Roentgenbild rb) throws RemoteException{
        System.out.println("Bericht: "+ rb.toString());

        Bericht antwort = new Bericht();
        antwort.setDiagnose("Kopfschmerzen");
        antwort.setWeiteresVorgehen("Ausruhen");

        System.out.println("Sende "+ antwort.toString() );
        return  antwort;
    }
}
