package de.lmu.api;

import de.lmu.entity.Bericht;
import de.lmu.entity.Roentgenbild;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface FrueherkennungsIF extends Remote {
    public Bericht analysieren(Roentgenbild rb) throws RemoteException;
}
