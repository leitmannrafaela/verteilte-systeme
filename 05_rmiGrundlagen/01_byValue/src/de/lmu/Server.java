package de.lmu;

import de.lmu.api.FrueherkennungsIF;
import de.lmu.impl.FrueherkennungsServer;

import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server {
    public static void main(String [] args) throws AlreadyBoundException {
        FrueherkennungsIF serverImpl = new FrueherkennungsServer();
        try {
            FrueherkennungsIF stub = (FrueherkennungsIF) UnicastRemoteObject.exportObject( serverImpl, 0);
            Registry reg = LocateRegistry.createRegistry(1099);
            reg.bind("Online", stub);
            System.out.println("Server ist bereit fuer Verbindung von Client...");
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
