package de.lmu.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

public class Roentgenbild implements Serializable {
    //Attribute
    private Date aufnahmeVom;
    private transient String privateName; //wird nicht serialisiert
    private byte[] rawData;

    public Roentgenbild() {
        this.aufnahmeVom = new Date();
        this.rawData = "Bild mit ".getBytes();
    }

    public void setPrivateName(String privateName) {
        this.privateName = privateName;
    }

    @Override
    public String toString() {
        return "Roentgenbild{" +
                "aufnahmeVom=" + aufnahmeVom +
                ", privateName='" + privateName + '\'' +
                ", rawData=" + Arrays.toString(rawData) +
                '}';
    }
}