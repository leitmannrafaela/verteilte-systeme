package de.client;

import de.lmu.api.FrueherkennungsIF;
import de.lmu.entity.Bericht;
import de.lmu.entity.Roentgenbild;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


public class Uniklinikum {
    public static void main(String [] args) {
        Roentgenbild rb = new Roentgenbild();
        rb.setPrivateName("Ahmad");
        try{
            Registry registry = LocateRegistry.getRegistry("localhost", 1099);
            FrueherkennungsIF stub = (FrueherkennungsIF) registry.lookup("Online");
            Bericht bericht = stub.analysieren(rb);
        }catch (RemoteException | NotBoundException e){
            e.printStackTrace();
        }
    }

}
