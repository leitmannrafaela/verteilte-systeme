import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

//passive Klasse
public class Parkhaus {
    private int gefuellt;
    private int belegtePlaetze = 0;
    private Lock monitor  = new ReentrantLock();
    Condition voll = monitor.newCondition();
    Condition leer = monitor.newCondition();

    public Parkhaus(int gefuellt) {
        this.gefuellt = gefuellt;
    }

    public void einfahren(){
        monitor.lock();
        try{
            while(belegtePlaetze == gefuellt){
                try{
                    System.out.println(Thread.currentThread().getName()+" bitte warten");
                    voll.await();
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
            belegtePlaetze += 1;
            leer.signal();
        }finally {
            monitor.unlock();
        }
    }

    public void ausfahren(){
        monitor.lock();
        try{
            belegtePlaetze -= 1;
        }finally {
            monitor.unlock();
        }
    }
}
