public class Class {

    public static void main (String [] args) throws InterruptedException {
        Parkhaus parkhaus = new Parkhaus(10);

        for(int i = 1; i <= 20; i++){
            Thread thread = new Thread(new Auto("R-RL-"+i,parkhaus));
            thread.setDaemon(true);
            thread.start();
        }

        Thread.sleep(3000);
        System.out.println("Ende der Simulation");
    }
}
