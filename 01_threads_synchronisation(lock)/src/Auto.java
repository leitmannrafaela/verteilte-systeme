import java.util.Random;

//aktive Klasse
public class Auto implements Runnable{
    private String kennzeichen;
    private Parkhaus pk;

    public Auto(String kennzeichen, Parkhaus pk) {
        this.kennzeichen = kennzeichen;
        this.pk = pk;
    }

    @Override
   public void run(){
       while(true){
           try {
               pk.einfahren();
               System.out.println("Einfahren "+ kennzeichen);
               long time = new Random().nextInt(10000);
               Thread.sleep(time);

               pk.ausfahren();
               System.out.println("Ausfahren "+kennzeichen);
               time = new Random().nextInt(10000);
               pk.ausfahren();
               Thread.sleep(time);

           } catch (InterruptedException e) {
               e.printStackTrace();
           }
       }
   }

}
