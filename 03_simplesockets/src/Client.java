import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String [] args){
        String SERVER_HOST = "localhost";
        int SERVER_PORT = 1234;

        System.out.println("Bitte geben Sie eine Nachricht ein:");
        Scanner sc = new Scanner(System.in);
        String TextFuerServer = sc.nextLine();

        try{
            Socket s = new Socket(SERVER_HOST,SERVER_PORT);

            InputStream in  = s.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            OutputStream out = s.getOutputStream();
            PrintWriter writer  = new PrintWriter(out);

            writer.println("Antwort von Server "+TextFuerServer);
            writer.flush();

            String antwortVonServer = reader.readLine();
            System.out.println("Antwort von Server " + antwortVonServer);

        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
