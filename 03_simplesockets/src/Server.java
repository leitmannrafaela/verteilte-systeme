import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String [] args){
        int port = 1234;

        try(ServerSocket serverSocket = new ServerSocket(port)){
            while(true){
                try{
                    System.out.println("Warte auf Verbindung...");
                    Socket s = serverSocket.accept();

                    InputStream in  = s.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                    OutputStream out = s.getOutputStream();
                    PrintWriter writer  = new PrintWriter(out);

                    writer.println("Hallo");
                    writer.flush();

                    String antwort = reader.readLine();
                    System.out.println("Neue Verbindung von "+ antwort);
                    s.close();
                }catch(IOException e){
                    e.printStackTrace();
                }
            }

        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
