package entity;

//passive Klasse
public class Parkhaus {

    private  int gefuellt;
    private int belegtePlaetze = 0;
    private Object monitor = new Object();

    public Parkhaus(int gefuellt) {
        this.gefuellt = gefuellt;
    }

    public void einfahren(){
        synchronized (monitor){
            while(belegtePlaetze == gefuellt){ //wartet bis ereignis passiert
                try{
                    System.out.println(Thread.currentThread().getName() + " bitte warten!");
                    monitor.wait();
                }catch (InterruptedException ex){
                    ex.printStackTrace();
                }
            }
            belegtePlaetze++;
        }
    }

    public void ausfahren(){
        synchronized (monitor){
            belegtePlaetze = belegtePlaetze-1;
            monitor.notify(); //geleichförmige autos (ein typ – deshalb nur notify)
        }
    }
}
