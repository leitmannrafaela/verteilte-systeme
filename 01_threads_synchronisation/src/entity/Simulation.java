package entity;

public class Simulation {

    public static void main(String [] args) throws InterruptedException{
        Parkhaus pk = new Parkhaus(10);

        for(int i=1; i<=20; i++){
            Thread thread = new Thread(new Auto("R-FH "+i, pk));
            thread.setDaemon(true);
            thread.setName("R-FH"+i);
            thread.start();
        }

        Thread.sleep(5000);
        System.out.println(Thread.currentThread().getName() + " Ende der Simulation!");
    }
}
